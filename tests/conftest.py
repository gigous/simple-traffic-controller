import pytest
from traffic_control import EE_WW, EN_WS, Simulation, VehicleSensors, TrafficController, Time, NN_SS, NW_SE, State
from typing import Dict

@pytest.fixture
def sim1():
    # setup
    s = Simulation()
    yield s
    # cleanup

@pytest.fixture
def sim2():
    # setup
    # SE NW
    s = Simulation(int('00000011', 2))
    yield s
    # cleanup

@pytest.fixture
def sim3():
    # setup
    vs_states: Dict[Time, State] = {
        Time(5): NN_SS + EN_WS + EE_WW,
    }
    s = Simulation(int('11111111', 2), vs_states)
    yield s
    # cleanup

@pytest.fixture
def sim4():
    # setup
    vs_states: Dict[Time, State] = {
        Time(65): NW_SE + EN_WS,
    }
    s = Simulation(int('11111111', 2), vs_states)
    yield s
    # cleanup


@pytest.fixture
def sim5():
    # setup
    # SE NW
    vs_states: Dict[Time, State] = {
        5:  0b01010100,
        9:  0b11110111,
        11: 0b11110100,
        33: 0b11111110,
        35: 0b11110011,
        38: 0b11110011,
        72: 0b11001111,
        # 73: 0b00011100,
        # 97: 0b11101011,
        # 99: 0b11101001,
    }
    s = Simulation(int('01010101', 2), vs_states)
    yield s
    # cleanup