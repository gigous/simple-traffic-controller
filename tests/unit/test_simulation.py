
from typing import Dict
from traffic_control import Simulation, NW_SE, NN_SS, EN_WS, EE_WW, State, Time, dir2lanes

def test_first_light_activates(sim1: Simulation):
    sim1.run(Time(1))
    assert sim1.tc.state == NW_SE

def test_light_no_traffic_no_timeout(sim1: Simulation):
    sim1.run(Time(480))
    assert sim1.tc.state == NW_SE

def test_light_two_lane_traffic_no_timeout(sim2: Simulation):
    sim2.run(Time(480))
    assert sim2.tc.state == NW_SE

def test_light_two_lane_min_timeout(sim3: Simulation):
    sim3.run(Time(11))
    assert sim3.tc.state == NN_SS

def test_light_all_lanes_max_timeout(sim4: Simulation):
    sim4.run(Time(60))
    assert sim4.tc.state == NW_SE
    sim4.run(Time(1))
    assert sim4.tc.state == NN_SS

def test_light_big_kahuna(sim5: Simulation):
    sim5.run(Time(16))
    assert sim5.tc.state == NW_SE
    sim5.run(Time(30))
    assert sim5.tc.state == NN_SS
    sim5.run(Time(1))
    assert sim5.tc.state == EN_WS
    sim5.run(Time(29))
    assert sim5.tc.state == EN_WS
    sim5.run(Time(1))
    assert sim5.tc.state == EE_WW

