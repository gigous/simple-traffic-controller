Traffic Intersection Control System
===================================

Run The Program
---------------

To get the environment ready to run the program:

```shell
python3 -m venv env
. env/bin/activate
pip install -r requirements.txt
python traffic_control.py
```

Assumptions
-----------

- Timesteps are in seconds
- All vehicles get through intersection at same speed (1 second)
- Do not  account for right turns
- Red (0) and Green (1) lights only
- For lane that traffic lights are Green for, if vehicle not sensed for 5 seconds and minimum active time reached, traffic light changes to next state
- If no vehicle sensed for lanes in next cycle, traffic lights for those lanes do not turn green
- Traffic light cycle order is strictly:
  1. NW & SE
  2. NN & SS
  3. EN & WS
  4. EE & WW
- Traffic lights always turn on in pairs according to the above

Components
----------

- Simulation
  - Vehicle Sensors
    - 1 per lane
    - Mocked to simulate traffic flow
  - Traffic Controller
    - 1 light per lane
    - changes based on vehicle sensors
    - controlled through Simulation

Design
------

The design chosen for this control system relies on an abstraction. Each lane is mapped to a number based on a power of 2:

- NE → 1
- SW → 2
- NN → 4
- SS → 8
- EN → 16
- WS → 32
- EE → 64
- WW → 128

When the traffic controller must change state, the simulation checks whether or not a vehicle is in either of the corresponding next lanes via bitwise AND. If no vehicles are detected, the simulation performs another bitwise AND on the next pair of lanes to check for detected vehicles. This process repeats until vehicles are detected in a pair of lanes or until all lanes have been checked, in which case the traffic controller does not change from its current state.

Tests
-----

Unit tests were written to verify the system and are located in `tests/`. To run tests:

```shell
# with virtual environment activated
python -m pytest tests/
```
