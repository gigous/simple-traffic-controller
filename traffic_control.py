from typing import Dict, NewType

State = NewType('LightState', int)
Time = NewType('Time', int)

# Traffic lane to value
dir2lanes = {
    "NW": 0b00000001,
    "SE": 0b00000010,
    "NN": 0b00000100,
    "SS": 0b00001000,
    "EN": 0b00010000,
    "WS": 0b00100000,
    "EE": 0b01000000,
    "WW": 0b10000000,
}

NO_VEHICLES_TIMEOUT = 5
NO_LIGHTS = State(0)
NW_SE = State(dir2lanes["NW"] + dir2lanes["SE"])
NN_SS = State(dir2lanes["NN"] + dir2lanes["SS"])
EN_WS = State(dir2lanes["EN"] + dir2lanes["WS"])
EE_WW = State(dir2lanes["EE"] + dir2lanes["WW"])
LIGHTS_UPPER_BOUND = 0b100000000 # 256

min_max_times = {
    NW_SE: (Time(10), Time(60)),
    NN_SS: (Time(30), Time(120)),
    EN_WS: (Time(10), Time(30)),
    EE_WW: (Time(30), Time(60)),
}

class Devices:
    """Represent device that controls or senses traffic"""
    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value: State):
        self._state = value

    def __init__(self, initial_state=0):
        self._state = initial_state

class VehicleSensors(Devices):
    pass

class TrafficController(Devices):
    @property
    def state(self):
        return super().state

    @state.setter
    def state(self, value: State):
        if value not in (NW_SE, NN_SS, EN_WS, EE_WW, NO_LIGHTS):
            raise ValueError("Invalid state for traffic controller")
        self._state = value

    @staticmethod
    def next_state(current_state: State) -> State:
        """Calculate next state based on a current state

        Parameters
        ----------
        current_state : State
            initial state of a controller

        Returns
        -------
        State
            Returns the next state in the traffic control cycle
        """
        if current_state == NO_LIGHTS:
            return NW_SE
        next = (current_state % LIGHTS_UPPER_BOUND) << 2
        if next > LIGHTS_UPPER_BOUND:
            return next >> 8
        return next

class Simulation:
    def __init__(self, vs_init_state: State=0, vs_states: Dict[Time, State]={}):
        """Constructor for simulation

        Parameters
        ----------
        vs_init_state : State, optional
            initial state of vehicle sensors, by default 0
        vs_states : Dict[Time, State], optional
            dictionary of timestep: state pairs
            at timestep provided by vs_states, vehicle sensors
            are changed by value at timestep key when simulation
            is run
        """
        self.vs_init_state: State = vs_init_state
        self.vs: VehicleSensors = VehicleSensors(vs_init_state)
        self.tc: TrafficController = TrafficController(NO_LIGHTS)
        self.ts: Time = Time(0)
        self.last_tc_state: State = State(0)
        self.ran: bool = False
        self.vs_states: Dict[Time, State] = vs_states

    def reset_simulation(self):
        """Resets simulation to initial state
        """
        self.vs.state = self.vs_init_state
        self.tc.state = NO_LIGHTS
        self.ts = 0
        self.last_tc_state = 0
        self.ran = False

    def run(self, timesteps: Time):
        """Run simulation for a certain amount of timesteps to
        simulate traffic flow

        Parameters
        ----------
        timesteps : Time
            Number of timesteps to run simulation for from current state
        """
        timestep = 0
        if not self.ran:
            self.next_state()
            # current state timestep
            self.cst: Time = Time(0)
            self.last_car_timer: Time = Time(0)
        while timestep < timesteps:
            tc_state: State = self.tc.state
            # if vehicle not sensed while corresponding traffic in either lane
            #   that traffic controller activated for, increment a timer
            # otherwise reset it
            if not tc_state & self.vs.state:
                self.last_car_timer += 1
            else:
                self.last_car_timer = 0

            # change vehicle sensors at specified timesteps
            if self.ts in self.vs_states:
                self.vs.state = self.vs_states[self.ts]

            # get min and max active times for current traffic controller state
            mn, mx = min_max_times[tc_state]

            # if cst has reached max time, or if traffic lights "timed out" at
            #   some point after min active time, transition to next state
            if self.cst >= mx or (self.last_car_timer >= NO_VEHICLES_TIMEOUT and mn <= self.cst):
                self.next_state()
                self.cst = Time(0)
                self.last_car_timer = Time(0)
            self.ts += 1
            self.cst += 1
            timestep += 1
            self.print_status()
        self.ran = True

    def next_state(self):
        ns: State = self.tc.state
        for _ in range(3):
            ns = TrafficController.next_state(ns)
            if ns & self.vs.state:
                self.tc.state = ns
                break
        else:
            self.tc.state = NW_SE

    # for debugging
    def print_status(self):
        ts = str(self.ts)
        if self.last_tc_state != self.tc.state:
            print(f"{f'{ts}:': <5}{int(bin(self.tc.state)[2:]):0>8}")
        self.last_tc_state = self.tc.state

if __name__ == "__main__":
    vs: State = 157
    s: Simulation = Simulation(vs, vs_states={10: dir2lanes["WW"] + NN_SS + dir2lanes["EN"], 20: dir2lanes["WW"] + dir2lanes["SS"] + dir2lanes["EN"]})
    s.run(50)
    s.reset_simulation()
    s.run(150)
